name := "scala_practice"

version := "1.0"

scalaVersion := "2.11.7"

scalacOptions += "-deprecation"

libraryDependencies ++= Seq(
  "org.scalaz" % "scalaz-core_2.11" % "7.1.4",
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "com.typesafe.akka" %% "akka-actor" % "2.4.0",
  "org.scalatest" % "scalatest_2.11" % "2.2.5" % "test",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4.0")