package com.akka_trafficsignals

/**
 * Created by kunaltiwary on 10/18/15.
 */

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, FSM}

import scala.concurrent.duration.FiniteDuration


// state -
sealed trait FSMSignal

case object RedSignal extends FSMSignal

case object YellowSignal extends FSMSignal

case object GreenSignal extends FSMSignal

// data
case class SignalColorData(str: String)

// event
case object ChangeSignal

case object RetainSignal

class SignalChangeFSMActor extends Actor with FSM[FSMSignal, SignalColorData] {

  // initial state of FSM
  startWith(RedSignal, SignalColorData("data"))
  implicit val timeout = FiniteDuration(5, TimeUnit.SECONDS)
  implicit val yellowTimeout = FiniteDuration(1, TimeUnit.SECONDS)


  when(RedSignal) {
    case Event(ChangeSignal, _) => goto(YellowSignal) forMax (timeout)
    case Event(RetainSignal, _) => stay()
  }

  when(YellowSignal) {
    case Event(ChangeSignal, _) => goto(GreenSignal) forMax (timeout)
    case Event(RetainSignal, _) => stay
  }

  when(GreenSignal) {
    case Event(ChangeSignal, _) => goto(RedSignal) forMax (timeout)
    case Event(RetainSignal, _) => stay
  }

  onTransition {
    case RedSignal -> YellowSignal => println("Changing from red to yellow signal - get ready to go")
    case YellowSignal -> GreenSignal => println("Changing from yellow to green signal - wroooom")
    case GreenSignal -> RedSignal => println("Changing from green to red signal - stop !!! :(")
  }

  initialize
}

