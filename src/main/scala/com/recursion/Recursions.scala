package com.recursion

import scala.annotation.tailrec

/**
 * Created by kunaltiwary on 10/24/15.
 */
object Recursions extends App {

  println("Factorial: " + factorial(7))

  println("Fibonacci: " + fibonacci(4))

  println("Sum: " + sum(6))


  def factorial(num: Int): Int = {
    @tailrec
    def go(num: Int, acc: Int): Int = {
      if (num <= 0) acc
      else go(num - 1, acc * num)
    }
    go(num, 1)
  }

  def fibonacci(num: Int): Int = {
    @tailrec
    def go(num: Int, a: Int = 0, b: Int = 1): Int = {
      if (num <= 0) a
      else go(num - 1, b, a + b)
    }
    go(num)
  }

  def sum(num: Int): Int = {
    @tailrec
    def go(num: Int, acc: Int): Int = {
      if (num <= 0) acc
      else go(num - 1, acc + num)
    }
    go(num, 0)
  }


}
