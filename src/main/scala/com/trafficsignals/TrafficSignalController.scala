//package com.trafficsignals
//
//import scala.concurrent.duration.FiniteDuration
//
//
///**
// * Created by kunaltiwary on 10/18/15.
// */
//object TrafficSignalController {
//
//  sealed trait TrafficEvent
//
//  case object ChangeSignal extends TrafficEvent
//
//  case object RetainSignal extends TrafficEvent
//
//  // Data is used while reacting to events
//  sealed trait Direction
//
//  case class NorthSouth extends Direction
//
//  case class EastWest extends Direction
//
//  sealed trait SignalColor
//
//  case class Red extends SignalColor
//
//  case class Yellow extends SignalColor
//
//  case class Green extends SignalColor
//
//  sealed trait Mode
//
//  case class Solid extends Mode
//
//  case class Off extends Mode
//
//  sealed trait FSMTrafficIntersection {
//    def signalColor: SignalColor
//
//    def direction: Direction
//  }
//
//  case class Signal(display: List[FSMTrafficIntersection])
//
//  object FSMTrafficIntersection {
//    def apply(signal: Signal): FSMTrafficIntersection = NSGreen(Green, NorthSouth)
//  }
//
//  case class NSGreen(signalColor: Green, direction: NorthSouth) extends FSMTrafficIntersection
//
//  case class NSYellow(signalColor: Yellow, direction: NorthSouth) extends FSMTrafficIntersection
//
//  case class NSRed(signalColor: Red, direction: NorthSouth) extends FSMTrafficIntersection
//
//  case class EWGreen(signalColor: Green, direction: EastWest) extends FSMTrafficIntersection
//
//  case class EWRed(signalColor: Red, direction: EastWest) extends FSMTrafficIntersection
//
//  case class EWYellow(signalColor: Yellow, direction: EastWest) extends FSMTrafficIntersection
//
//
//  val fsm =
//    FSM[TrafficEvent, FSMTrafficIntersection] {
//      case (ChangeSignal, NSRed()) =>
//        EWRed(FSMTrafficIntersection.redgreenTimeout)
//
//      case (ChangeSignal, NSYellow(FSMTrafficIntersection.yellowTimeout)) =>
//      EWRed(FSMTrafficIntersection.redgreenTimeout)
//
//      case (ChangeSignal, NSRed(FSMTrafficIntersection.redgreenTimeout))  =>
//      EWGreen(FSMTrafficIntersection.redgreenTimeout)
//
//      case (ChangeSignal, NSRed(FSMTrafficIntersection.redgreenTimeout))  =>
//      EWYellow(FSMTrafficIntersection.yellowTimeout)
//
//    }
//
//  //  sealed trait TrafficSignalIntersection {
//  //
//  //  }
//
//}
