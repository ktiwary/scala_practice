package com.trafficsignals

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.FiniteDuration


/**
 * Created by kunaltiwary on 10/17/15.
 */
object Signaling {

  sealed trait Event

  case object SignalTimeout extends Event

  sealed trait SignalDirection {
    def timeDelay: FiniteDuration
  }

  case class NSGreen(timeDelay: FiniteDuration) extends SignalDirection

  case class NSYellow(timeDelay: FiniteDuration) extends SignalDirection

  case class NSRed(timeDelay: FiniteDuration) extends SignalDirection

  case class EWGreen(timeDelay: FiniteDuration) extends SignalDirection

  case class EWRed(timeDelay: FiniteDuration) extends SignalDirection

  case class EWYellow(timeDelay: FiniteDuration) extends SignalDirection

  sealed trait Mode

  case object Off extends Mode

  case object Solid extends Mode

  case class Signal(isOperational: Boolean, display: Map[SignalDirection, Mode])

  object Signal {

    import scalaz.syntax.state._
    import scalaz.State, State._

    type ->[A, B] = (A, B)
    type SignalState[A] = State[SignalDirection, A]

    val default = Signal(
      isOperational = false,
      display = Map(NSRed(timeout) -> Solid, EWRed(timeout) -> Solid))

    // make the signal available for use
    def enable: State[Signal, Boolean] =
      for {
        a <- init
        _ <- modify((s: Signal) => s.copy(isOperational = true))
        r <- get
      } yield r.isOperational

    //TODO: requires validation to prevent invalid state changes
    private def display(seq: Seq[SignalDirection -> Mode]): Signal => Signal = signal =>
      if (signal.isOperational)
        signal.copy(display = signal.display ++ seq.toMap)
      else default

    private def change(seq: SignalDirection -> Mode*): State[Signal, Map[SignalDirection, Mode]] =
      for {
        m <- init
        _ <- modify(display(seq))
        signal <- get
      } yield signal.display

    // combinators for valid states the signal can be in.
    implicit val timeout = FiniteDuration(5, TimeUnit.MINUTES)
    implicit val yellowTimeout = FiniteDuration(30, TimeUnit.SECONDS)

    def halt = change(NSRed(timeout) -> Solid, NSYellow(yellowTimeout) -> Off, NSGreen(timeout) -> Off)

    def ready = change(NSRed(timeout) -> Solid, NSYellow(yellowTimeout) -> Solid, NSGreen(timeout) -> Off)

    def go = change(NSRed(timeout) -> Off, NSYellow(yellowTimeout) -> Off, NSGreen(timeout) -> Solid)

    def slow = change(NSRed(timeout) -> Off, NSYellow(yellowTimeout) -> Solid, NSGreen(timeout) -> Off)
  }

  def main(args: Array[String]): Unit = {
    import Signal._
    import scalaz.State.{get => current}

    val program = for {
      _ <- enable
      r0 <- current // debuggin
      _ <- halt
      r1 <- current // debuggin
      _ <- ready
      r2 <- current // debuggin
      _ <- go
      r3 <- current // debuggin
      _ <- slow
      r4 <- current
    } yield r0 :: r1 :: r2 :: r3 :: r4 :: Nil

    program.eval(default).zipWithIndex.foreach { case (v, i) =>
      println(s"r$i - $v")
    }
  }


}
