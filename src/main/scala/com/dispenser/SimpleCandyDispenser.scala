package com.dispenser

/**
 * Created by kunaltiwary on 10/17/15.
 */
object SimpleCandyDispenser {

  sealed trait Input

  case object Coin extends Input

  case object Turn extends Input

  sealed trait Machine {
    def candies: Int

    def coins: Int
  }

  object Machine {
    def apply(candies: Int, coins: Int): Machine = LockedMachine(candies, coins)
  }

  case class LockedMachine(candies: Int, coins: Int) extends Machine

  case class UnlockedMachine(candies: Int, coins: Int) extends Machine

  val fsm =
    FSM[Input, Machine] {
      case (Coin, LockedMachine(candies, coins)) if candies > 0 =>
        UnlockedMachine(candies, coins + 1)

      case (Turn, UnlockedMachine(candies, coins)) if candies > 0 =>
        LockedMachine(candies - 1, coins)
    }

}
