package com.hofs

import scala.annotation.tailrec

/**
 * Created by kunaltiwary on 10/25/15.
 */
object HigherOrderFunctions extends App {

  print(isSorted(Array(1, 2, 3, 4), (x: Int, y: Int) => x > y))

  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    @tailrec
    def loop(n: Int): Boolean = if (n >= as.length - 1) true
    else if (ordered(as(n), as(n + 1))) false
    else loop(n + 1)

    loop(0)
  }

  def curry[A, B, C](f: (A, B) => C): A => (B => C) = (a: A) => (b: B) => f(a: A, b: B)

  def uncurry[A, B, C](f: A => B => C): (A, B) => C = (a: A, b: B) => f(a: A)(b: B)

  def compose[A, B, C](f: B => C, g: A => B): A => C = (a: A) => f(g(a: A))

}
