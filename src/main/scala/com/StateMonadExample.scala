package com

object StateMonadExample extends App {

  case class SimpleGenerator(var seed: Int) {
    def generateRandomInt(): Int = {
      val rand = seed + 5
      seed = seed + 1
      rand
    }
  }

  val gen = SimpleGenerator(30)
  //println(gen.generateRandomInt())


  case class GeneratorState(seed: Int)

  object GeneratorState {
    def generateRandom(g: GeneratorState): (GeneratorState, Int) = {
      (GeneratorState(g.seed + 1), g.seed + 5)
    }
  }

  import GeneratorState._

  val state = GeneratorState(30)
  private val tuple = generateRandom(state)
  tuple._2

  case class Generator(seed: Int)

  import scalaz._
  import Scalaz._

  object GeneratorStateMonad {
    def generateRandom(): State[Generator, Int] = {
      for {
        g <- get[Generator]
        _ <- put(Generator(g.seed + 1))
      } yield g.seed + 5

    }
  }

  private val value = GeneratorStateMonad.generateRandom()
  value.map { c =>
    println(s"State Monad Value $c")
  }

}
