package com.wordcount

/**
 * Created by kunaltiwary on 10/17/15.
 */
object WordCount {

  def main(args: Array[String]) {
    println(countWords("apple,apple,banana,banana,mango,grapes", Map.empty))
  }

  def words(str: String) = str.split(",")

  def countWords(str: String, currMap: Map[String, Int]): Map[String, Int] = {
    words(str).foldLeft(currMap) { (map, word) =>
      val count = map.getOrElse(word, 0) + 1
      map + (word -> count)
    }
  }

}
