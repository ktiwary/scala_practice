package com.wordcount

object Staircase extends App {
  val x = 123

  def outerFunction() = {
    val x = 456
    println(innerFunction())

    def innerFunction() = {
      x
    }
  }
}
