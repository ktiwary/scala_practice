package com.funcdatastruc

/**
 * Created by kunaltiwary on 10/25/15.
 */

import com.funcdatastruc.List._

object FunctionalDataStructures extends App {

  val x = List(1, 2, 3, 4, 5) match {
    case Cons(x, Cons(2, Cons(4, _))) => x
    case Nil => 42
    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    case Cons(h, t) => h + sum(t)
    case _ => 101
  }

  println(x)

  println(tail(List(1, 2, 3, 4)))
  println(setHead(List(1, 2, 3, 4), 5))
  println(drop(List(1, 2, 3, 4), 2))
  println(dropWhile[Int](List[Int](1, 2, 3, 4), (x: Int) => x % 2 == 0))
}
