package com.akka_trafficsignals

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestFSMRef, TestKit}
import org.scalatest._


/**
 * Created by kunaltiwary on 10/18/15.
 */
class SignalChangingFSMTest(as: ActorSystem) extends TestKit(as)
with ImplicitSender
with WordSpecLike
with Matchers
with BeforeAndAfterAll {

  val sigData = SignalColorData("data")
  val fsmRef = TestFSMRef(new SignalChangeFSMActor)

  def this() = this(ActorSystem("testActorSystem"))

  "Our signal emulating FSMActor" must {
    "be red at initial stage " in {
      assert(fsmRef.stateName == RedSignal)
      assert(fsmRef.stateData == sigData)
    }

    "change to yellow after AlternateColour event occurred " in {
      fsmRef ! ChangeSignal
      assert(fsmRef.stateName == YellowSignal)
      assert(fsmRef.stateData == sigData)
    }

    "change to green after AlternateColour event occurred" in {
      fsmRef ! ChangeSignal
      assert(fsmRef.stateName == GreenSignal)
      assert(fsmRef.stateData == sigData)
    }

    "retain should not change the fsm state" in {
      fsmRef ! RetainSignal
      assert(fsmRef.stateName == GreenSignal)
      assert(fsmRef.stateData == sigData)
    }
  }

  override def afterAll = {
    println("traffic is clear now - obey traffic rules")
    print(fsmRef.stateName)
    print(fsmRef.stateData)
  }


}
